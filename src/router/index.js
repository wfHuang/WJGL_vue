import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login/Login'
import Regist from '@/components/Login/Regist'
import P404 from '@/components/404/404'
import Loading from '@/components/404/Loading'
import Main from '@/components/Main'
import Menu from '@/components/Basic/Menu'  //菜单管理

Vue.use(Router)

export default new Router({
  mode: 'hash',//history
  routes: [
    {
      path: '/login',//登录页
      name: 'Login',
      component: Login
    },
    {
      path: '/regist',//注册页
      name: 'Regist',
      component: Regist
    },
    {
      path: '/fileview',//文件预览
      component: resolve => require(['@/components/Basic/FileView'],resolve),
      meta: {
        requireAuth: true,
        keepAlive: false, //此组件不需要被缓存
      },
    },
    {
      path: '/',//首页
      name: 'Main',
      component: Main,
      redirect: '/filelist',//默认子路由
      meta: {
        requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
        keepAlive: false, //此组件不需要被缓存
      },
      children: [
        {
          path: 'user',//用户管理
          component: resolve => require(['@/components/Basic/User'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          },
        },
        {
          path: 'role',//角色管理
          component: resolve => require(['@/components/Basic/Role'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          },
        },
        {
          path: 'department',//部门
          component: resolve => require(['@/components/Basic/Department'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          },
        },
        {
          path: 'rpermissions',//角色权限设置
          component: resolve => require(['@/components/Basic/RPermissions'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          },
        },
        {
          path: 'dpermissions',//部门权限设置
          component:  resolve => require(['@/components/Basic/DPermissions'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          },
        },
        {
          path: 'classSet',//文件分类权限设置
          component:  resolve => require(['@/components/Basic/ClassSet'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          },
        },
        {
          path: 'rolelist',//角色成员列表
          component:  resolve => require(['@/components/Basic/RoleList'],resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: 'classList',//分类成员列表
          component: resolve => require(['@/components/Basic/ClassList'],resolve),
          meta: {
            requireAuth: true,
          },
        },
        {
          path: 'deparmentList',//部门成员列表
          component:  resolve => require(['@/components/Basic/DeparmentList'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          },
        },
        {
          path: 'loglogin',//登录日志
          component:  resolve => require(['@/components/Basic/LogLogin'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          },
        },
        {
          path: 'logoperation',//操作日志
          component:  resolve => require(['@/components/Basic/LogOperation'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          },
        },
        {
          path: 'logfile',//文件日志
          component: resolve => require(['@/components/Basic/LogFile'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          },
        },
        {
          path: 'class',//文件分类
          component: resolve => require(['@/components/Basic/Class'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          },
        },
        {
          path: 'setfile',//文件上传
          component:  resolve => require(['@/components/Basic/SetFile'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          },
        },
        {
          path: 'fileaudit',//文件审核
          component:  resolve => require(['@/components/Basic/FileAudit'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          },
        },
        {
          path: 'filelist',//文件列表
          component:  resolve => require(['@/components/Basic/FileList'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: true, // 此组件需要被缓存
          },
        },

        {
          path: 'Loading',//等待页面
          component: resolve => require(['@/components/404/Loading'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          },
        },
        {
          path: 'statistics',//工作台
          component: resolve => require(['@/components/Basic/Statistics'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          }
        },
        {
          path: 'menu',//菜单管理
          component: Menu,
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          }
        },
        {
          path: 'comments',//留言列表
          component:  resolve => require(['@/components/Basic/Comments'],resolve),
          meta: {
            requireAuth: true,
            keepAlive: false, //此组件不需要被缓存
          }
        }
      ]
    },
    { path: '*', component: P404 }
  ]
})


